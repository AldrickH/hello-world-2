@extends('base')

@section('body')
<table border='1'>
    <tr>
        <td>ID</td>
        <td>Nama</td>
        <td>Komentar</td>
        <td>Action</td>
    </tr>

    @foreach ($komentars as $komentar)
    <tr>
        <td>{{ $komentar->id }}</td>
        <td>{{ $komentar->nama }}</td>
        <td>{{ $komentar->komentar }}</td>
        <td><a href="{{ route('komentarDetail', ['id' => $komentar->id])}}"><button>DETAIL</button></a></td>
        <td><a href="{{ route('komentarEditForm', ['id' => $komentar->id])}}"><button>EDIT</button></a></td>
        <td><a href="{{ route('komentarDelete', ['id' => $komentar->id])}}"><button onclick="return confirm('Are you sure ?')">DELETE</button></a></td>
    </tr>
    @endforeach

</table>

<a href="{{ route('komentarNewForm') }}"><button>Add new Komentar</button></a>
@endsection