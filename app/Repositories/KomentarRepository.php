<?php

namespace App\Repositories;

use App\Komentar;

class KomentarRepository implements KomentarInterface
{
    public function create(string $nama, string $komentar)
    {
        $newKomentar = new Komentar;
        $newKomentar->nama = $nama;
        $newKomentar->komentar = $komentar;
        $newKomentar->save();
    }

    public function all()
    {
        return Komentar::all();
    }

    public function get(int $id)
    {
        return Komentar::findOrFail($id);
    }

    public function update(int $id, Komentar $data)
    {
        $komentar = Komentar::findOrFail($id);
        $komentar->nama = $data->nama;
        $komentar->komentar = $data->komentar;
        $komentar->save();
    }
    
    public function delete(int $id)
    {
        $komentar = Komentar::findOrFail($id);
        $komentar->delete();
    }
}
