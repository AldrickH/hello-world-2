<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// use App\Repositories\KomentarInterface;
// use App\

class BackendServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Repositories\KomentarInterface', 'App\Repositories\KomentarRepository');
    }    
}
