<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\

class Hello extends Controller
{
    public function Index()
    {
        return view('hello', ['name' => 'world']);
    }

    public function show($name)
    {
        return view('hello', ['name' => $name]);
    }
}
